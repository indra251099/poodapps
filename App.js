import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Project pertamaku menggunakan Expo</Text>
      <Text>Belajar react native / framework Javascript</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FEF400',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'blue',
  },
});
